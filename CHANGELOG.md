# Changelog
All notable changes to this project will be documented in this file

The format  is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0)
The versionining of this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html)

## [Unreleased]
### Added
- Basic project structure for Apple, Android, and Web
- Python script to run formatting, linting, and testing automatically
- Blog post associated with article
- MIT licenses
