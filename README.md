# Goute

## One App Three Ways

-- badges --

![final_version](Goute-Blog/Resources/images/oatw/oatw.png)

## Installation
There are several required components for this to work.
1. The latest version of Xcode [here](https://apps.apple.com/us/app/xcode/id497799835?mt=12)
2. The Canary version of Android Studio (I'm using the M1 build) [here](https://redirector.gvt1.com/edgedl/android/studio/ide-zips/2021.2.1.7/android-studio-2021.2.1.7-mac_arm.zip)
3. The latest version of Visual Studio Code [here](https://code.visualstudio.com/sha/download?build=stable&os=darwin-universal)

## Usage

## Support

## Roadmap

## Contributing

## Authors and Acknowledgements

## License
[MIT](https://choosealicense.com/licenses/mit/)