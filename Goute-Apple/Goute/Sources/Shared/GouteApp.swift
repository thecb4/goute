//
//  GouteApp.swift
//  Shared
//
//  Created by Cavelle Benjamin on 12/26/21.
//

import SwiftUI

@main
struct GouteApp: App {
  var body: some Scene {
    WindowGroup {
      ContentView()
    }
  }
}
