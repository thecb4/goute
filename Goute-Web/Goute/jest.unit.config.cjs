const config = {
	testEnvironment: 'jsdom',
	transform: {
		'^.+\\.js$': 'babel-jest',
		// https://daveceddia.com/svelte-typescript-jest/
		'^.+\\.ts$': 'ts-jest',
		'^.+\\.svelte$': 'svelte-jester'
	},
	moduleFileExtensions: ['ts', 'js', 'svelte'],
	setupFilesAfterEnv: ['@testing-library/jest-dom/extend-expect']
};

module.exports = config;
