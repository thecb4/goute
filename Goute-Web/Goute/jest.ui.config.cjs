const config = {
	preset: 'jest-playwright-preset',
	// "testEnvironment": "jsdom",
	transform: {
		'^.+\\.ts$': 'ts-jest',
		'^.+\\.js$': 'babel-jest',
		'^.+\\.svelte$': 'svelte-jester'
	},
	moduleFileExtensions: ['ts', 'js', 'svelte'],
	setupFilesAfterEnv: ['@testing-library/jest-dom/extend-expect']
};

module.exports = config;
