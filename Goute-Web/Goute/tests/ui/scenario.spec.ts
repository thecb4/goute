import { chromium } from 'playwright';
import { firefox } from 'playwright';
import { webkit } from 'playwright';

describe('Behavior across different browsers', () => {
	it('should work on Chromium', async () => {
		const browser = await chromium.launch();
		const page = await browser.newPage();

		await page.goto('http://localhost:3000/');
		expect(await page.title()).toBe('Goûte');

		await page.close();
		await browser.close();
	});

	it('should work on Firefox', async () => {
		const browser = await firefox.launch();
		const page = await browser.newPage();

		await page.goto('http://localhost:3000/');
		expect(await page.title()).toBe('Goûte');

		await page.close();
		await browser.close();
	});

	it('should work on Webkit', async () => {
		const browser = await webkit.launch();
		const page = await browser.newPage();

		await page.goto('http://localhost:3000/');
		expect(await page.title()).toBe('Goûte');

		await page.close();
		await browser.close();
	});
});
