import { render } from '@testing-library/svelte';
import Index from '../../src/routes/index.svelte';

test('should render', () => {
	//const results = render(Index, { props: { name: "world" } });
	const results = render(Index);

	expect(() => results.getByText('Welcome to SvelteKit')).not.toThrow();
});
